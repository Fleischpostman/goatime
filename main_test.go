package main

import (
	"os"
	"testing"
)

func TestInitApp(t *testing.T) {

	InitApp()

	// NOTE(Fabi): Condition 1: We expect to have a settings file after InitApp runs
	settingsFilePath := BuildAppConfigFileName()
	_, err := os.Stat(settingsFilePath)
	if err != nil {
		t.Fatalf("No config file exists. Init should create a config file if it"+
			" can't finde one. Expected location of config file: %s", settingsFilePath)
	}
}
