package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"github.com/AllenDang/giu"
	"github.com/google/uuid"
	"io"
	"log"
	"os"
	"time"
)

type AppSettings struct {
	DataSyncLocation      string
	SaveIntervalInMinutes int32
}

type AppState struct {
	Projects                 []Project
	ProjectInEditMode        *Project
	SettingsInEditMode       AppSettings
	SecondsPastSinceLastSave int32
}

type Project struct {
	Id             uuid.UUID
	Parent         uuid.UUID
	Children       []uuid.UUID
	Name           string
	NameInEditMode string
	IsRunning      bool
	TimesStarted   []time.Time
	TimesEnded     []time.Time
}

func MakeProject() Project {
	return Project{
		Id:           uuid.New(),
		Name:         "Project",
		Children:     make([]uuid.UUID, 0),
		TimesStarted: make([]time.Time, 0),
		TimesEnded:   make([]time.Time, 0)}
}

func (project *Project) CalculateTotalTime() time.Duration {

	// NOTE(Fabi): In case the project is running we add on time point to the end
	// in order to make the calculations, at the end of the function we then have to
	// remove it again. I guessed this design makes the calculation easier than
	// handling the last case differently in the for loop.
	if project.IsRunning {
		project.TimesEnded = append(project.TimesEnded, time.Now())
		defer func() { project.TimesEnded = project.TimesEnded[:(len(project.TimesEnded) - 1)] }()
	}

	if len(project.TimesStarted) != len(project.TimesEnded) {
		panic("Can't calculate total time. There is a miss match in the amount of time tracking has been started and ended")
	}

	var total time.Duration
	for index := range project.TimesStarted {
		start := project.TimesStarted[index]
		end := project.TimesEnded[index]
		elapsed := end.Sub(start)
		total += elapsed
	}

	return total
}

const (
	AppName = "tracker"
)

// The whole global state of the application
var (
	app      AppState
	settings AppSettings
)

func LoadAppDataFromDisk(filepath string) {
	if _, err := os.Stat(filepath); err == nil {
		bytes, err := os.ReadFile(filepath)
		if err != nil {
			log.Fatal(err)
		}

		err = json.Unmarshal(bytes, &app)
		if err != nil {
			log.Printf("Failed to load app data from %s. Current app state is "+
				"not changed\n", filepath)
		}
	} else if !errors.Is(err, os.ErrNotExist) {
		// NOTE(Fabi): If we reach this scenario the file does not exist,
		// but another error than the file not found error is returned, so
		// we must be careful
		panic(err)
	}
}

func AppSettingsModal(modalName string) *giu.PopupModalWidget {
	return giu.PopupModal(modalName).Layout(
		giu.InputText(&app.SettingsInEditMode.DataSyncLocation).Label(
			"Data Sync Directory").Size(400),
		giu.InputInt(&app.SettingsInEditMode.SaveIntervalInMinutes).Label(
			"Save interval (minutes)").Size(50),
		giu.Align(giu.AlignCenter).To(giu.Row(
			giu.Button("Yes").OnClick(ApplyAppSettingsChanges),
			giu.Button("Cancel").OnClick(func() { giu.CloseCurrentPopup() }),
		)),
	)
}

func ApplyAppSettingsChanges() {

	defer giu.CloseCurrentPopup()

	if settings.DataSyncLocation != app.SettingsInEditMode.DataSyncLocation {
		LoadAppDataFromDisk(app.SettingsInEditMode.DataSyncLocation + "/projects.json")
	}

	settings = app.SettingsInEditMode
	settingsPath := BuildAppConfigDirPath()

	if _, err := os.Stat(settingsPath); os.IsNotExist(err) {
		if err = os.MkdirAll(settingsPath, os.ModePerm); err != nil {
			log.Fatal(err)
		}
	}

	bytes, err := json.Marshal(settings)
	if err != nil {
		fmt.Println("Could not save settings. Cancel this operation.")
		log.Println(err)
	}

	err = os.WriteFile(BuildAppConfigFileName(), bytes, 0666)
	if err != nil {
		fmt.Println("Could not save settings. Cancel this operation.")
		log.Println(err)
	}
}

func ProjectWidget(_ int, elem any) giu.Widget {

	project := elem.(*Project)
	var startStopButtonLabel string
	if project.IsRunning {
		startStopButtonLabel = "Stop"
	} else {
		startStopButtonLabel = "Start"
	}

	defaultLabelLayout := giu.Layout{
		giu.Label(project.Name),
		giu.Event().OnDClick(giu.MouseButtonLeft, func() {
			project.NameInEditMode = project.Name
			app.ProjectInEditMode = project
		}),
	}

	editModeLabelLayout := giu.Layout{
		giu.InputText(&project.NameInEditMode),
		giu.Button("Accept").OnClick(func() {
			project.Name = project.NameInEditMode
			app.ProjectInEditMode = nil
		}),
		giu.Button("Cancel").OnClick(func() {
			app.ProjectInEditMode = nil
		}),
	}

	var timeSpentStr string
	if len(project.Children) == 0 {
		timeSpentStr = fmt.Sprintf("Total Time: %s", project.CalculateTotalTime().Round(time.Second))
	} else {
		timeSpentInSelf := project.CalculateTotalTime().Round(time.Second)
		var timeSpentInChildren time.Duration
		for _, childId := range project.Children {
			subproject := Find(app.Projects, func(p Project) bool {
				return p.Id == childId
			})

			timeSpentInSubproject := subproject.CalculateTotalTime().Round(time.Second)
			timeSpentInChildren += timeSpentInSubproject
		}

		timeSpentStr = fmt.Sprintf("In project: %s, In subprojects: %s, Total %s",
			timeSpentInSelf,
			timeSpentInChildren,
			timeSpentInSelf+timeSpentInChildren)
	}

	var anySlice []interface{}
	for _, id := range project.Children {
		subproject := Find(app.Projects, func(p Project) bool {
			return p.Id == id
		})

		anySlice = append(anySlice, subproject)
	}

	var subprojectTree giu.Layout
	if len(project.Children) == 0 {
		subprojectTree = giu.Layout{}
	} else {
		label := fmt.Sprintf("%s subprojects", project.Name)
		subprojectTree = giu.Layout{
			giu.TreeNode(label).Layout(
				giu.RangeBuilder(fmt.Sprintf("Subprojects%s", project.Id), anySlice,
					ProjectWidget),
			),
		}
	}

	layout := giu.Layout{
		giu.Row(
			giu.Condition(project == app.ProjectInEditMode, editModeLabelLayout, defaultLabelLayout),
			giu.Label(timeSpentStr),
			giu.Button(startStopButtonLabel).OnClick(func() {

				project.IsRunning = !project.IsRunning

				if !project.IsRunning {
					project.TimesEnded = append(project.TimesEnded, time.Now())
				} else {
					project.TimesStarted = append(project.TimesStarted, time.Now())
				}
			}),
			giu.Button("Delete").OnClick(func() {

				projectIndex := IndexOf(app.Projects, func(p Project) bool {
					return p.Id == project.Id
				})

				if project.Parent != uuid.Nil {
					parentProject := Find(app.Projects, func(p Project) bool {
						return p.Id == project.Parent
					})

					indexInParent := IndexOf(parentProject.Children, func(u uuid.UUID) bool {
						return u == project.Id
					})

					parentProject.Children = append(parentProject.Children[:indexInParent],
						parentProject.Children[indexInParent+1:]...)

				}

				app.Projects = append(app.Projects[:projectIndex], app.Projects[projectIndex+1:]...)
			}),
			giu.Button("Add sub-project").OnClick(func() {
				subProject := MakeProject()
				subProject.Parent = project.Id
				project.Children = append(project.Children, subProject.Id)
				app.Projects = append(app.Projects, subProject)
			}),
		),
		subprojectTree,
	}

	return layout
}

func InitApp() {

	cliSettingsPath := flag.String("settings-file", "", "path to a settings file, will overwrite the default location")
	flag.Parse()

	var settingsFilePath string
	if *cliSettingsPath == "" {
		settingsFilePath = BuildAppConfigFileName()
	} else {
		settingsFilePath = *cliSettingsPath
	}

	loadingSettingsSucceeded := false

	if _, err := os.Stat(settingsFilePath); err == nil {
		bytes, err := os.ReadFile(settingsFilePath)
		if err != nil {
			log.Fatal(err)
		}

		// Note(Fabi): It might be possible that the file was created but
		// still is empty. In this case we skip the loading as well.
		if len(bytes) != 0 {

			err = json.Unmarshal(bytes, &settings)
			if err != nil {
				log.Fatal(err)
			}

			loadingSettingsSucceeded = true
		}

	} else if errors.Is(err, os.ErrNotExist) {

		settingsDir := BuildAppConfigDirPath()
		if _, err := os.Stat(settingsDir); os.IsNotExist(err) {
			if err = os.MkdirAll(settingsDir, os.ModePerm); err != nil {
				log.Fatal(err)
			}
		}

		f, err := os.Create(settingsFilePath)
		if err != nil {
			log.Fatal(err)
		}

		defer func(f *os.File) {
			err := f.Close()
			if err != nil {
				log.Fatal(err)
			}
		}(f)

	} else {
		log.Fatal(err)
	}

	if loadingSettingsSucceeded && settings.DataSyncLocation != "" {
		LoadAppDataFromDisk(settings.DataSyncLocation + "/projects.json")
	}
}

func loop() {

	// NOTE(Fabi): We have to convert this to an any slice, otherwise we can't put
	// it into the RangeBuilder. Also, we only draw the projects that are master projects
	// subprojects will be drawn as der children.
	var anySlice []interface{}
	for index := range app.Projects {
		project := app.Projects[index]
		if project.Parent == uuid.Nil {
			anySlice = append(anySlice, &app.Projects[index])
		}
	}

	giu.SingleWindowWithMenuBar().Layout(
		giu.MenuBar().Layout(
			giu.MenuItem("New Project").OnClick(func() { app.Projects = append(app.Projects, MakeProject()) }),
			giu.MenuItem("Settings").OnClick(func() {
				app.SettingsInEditMode = settings
				giu.OpenPopup("Settings")
			}),
			AppSettingsModal("Settings"),
			giu.Condition(settings.DataSyncLocation != "",
				giu.Layout{},
				giu.Layout{giu.Spacing(), giu.Label("Sync Location is not configured! Please open settings! Otherwise your data will be lost!")},
			),
		),

		giu.Label("Projects:"),
		giu.RangeBuilder("Projects", anySlice, ProjectWidget),
	)
}

func Update() {
	defer func() {
		if x := recover(); x != nil {
			log.Printf("run time panic: %v", x)
			panic(x)

		}
	}()

	for range time.Tick(time.Second * 1) {
		giu.Update()

		app.SecondsPastSinceLastSave += 1

		if settings.SaveIntervalInMinutes != 0 {
			HandleAutoSave()
		}
	}
}

func HandleAutoSave() {

	saveIntervalInSeconds := settings.SaveIntervalInMinutes * 60

	if app.SecondsPastSinceLastSave >= saveIntervalInSeconds {
		app.SecondsPastSinceLastSave = 0

		projectsToResume := make([]uuid.UUID, 0)
		for _, project := range app.Projects {
			if project.IsRunning {
				projectsToResume = append(projectsToResume, project.Id)
			}
		}

		SaveAppState()

		for index := range app.Projects {
			project := &app.Projects[index]

			for _, id := range projectsToResume {
				if project.Id == id {
					project.IsRunning = true
					project.TimesEnded = project.TimesEnded[:len(project.TimesEnded)-1]
				}
			}
		}
	}
}

func BuildAppStateStorageFilePath() string {
	return settings.DataSyncLocation + "/projects.json"
}

func BuildAppConfigDirPath() string {
	configDir, err := os.UserConfigDir()
	if err != nil {
		// NOTE(Fabi): This might be a problem of a bad configured machine, most
		// likely not of the app. Therefore, we just throw the error and close
		// the app without any error handling on our side.
		log.Println("No app data directory was configured for your user. The app can't find a location to store the app settings that way.")
		log.Fatal(err)
	}

	return fmt.Sprintf("%s/%s", configDir, AppName)
}

func BuildAppConfigFileName() string {
	appConfigDir := BuildAppConfigDirPath()
	return appConfigDir + "/config.json"
}

func SaveAppState() {

	log.Println("Saved state")

	// NOTE(Fabi): End all running timers in case there are any
	for index := range app.Projects {
		project := &app.Projects[index]

		if project.IsRunning {
			project.TimesEnded = append(project.TimesEnded, time.Now())
			project.IsRunning = false
		}
	}

	bytes, err := json.Marshal(app)
	if err != nil {
		log.Fatal("Failed to serialize app state", err)
	}

	filepath := BuildAppStateStorageFilePath()
	if os.WriteFile(filepath, bytes, 0666) != nil {
		log.Fatal("Failed to write app state to file", err)
	}
}

func main() {

	defer func() {
		SaveAppState()

		if x := recover(); x != nil {
			log.Printf("run time panic: %v", x)
			panic(x)

		}
	}()

	logFilePath := fmt.Sprintf("%s/%s", BuildAppConfigDirPath(), "log.txt")
	logFile, err := os.OpenFile(logFilePath, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)
	if err != nil {
		panic(err)
	}
	defer func(logFile *os.File) {
		_ = logFile.Close()
	}(logFile)
	mw := io.MultiWriter(os.Stdout, logFile)
	log.SetOutput(mw)

	InitApp()

	// NOTE(Fabi): Run app
	wnd := giu.NewMasterWindow("Goatime", 800, 600, 0)
	go Update()
	wnd.Run(loop)
}

func Find[T any](slice []T, predicate func(T) bool) *T {

	for index := range slice {
		element := &slice[index]
		if predicate(*element) {
			return element
		}
	}

	return nil
}

func IndexOf[T any](slice []T, predicate func(T) bool) int {
	for index, elem := range slice {
		if predicate(elem) {
			return index
		}
	}

	return -1
}
