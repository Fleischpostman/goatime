# Goatime

A small and little time tracking app I built for myself and for learning purposes.
It works as "leightweight" desktop app and you can do sync about devices by configuring data location as a shared drive (e.g. cloud storage synced across your devices).
Probably not useful for anyone else but feel free to fork or extend the app if you want more from it.

## Development

This project uses [giu](https://github.com/AllenDang/giu) Framework as I wanted to test this kind of GUI solution for Go projects.
If you want to extend the app you probably need to build the Dear ImGui references of giu first.
Check the repository for further instructions.
